# | BASE |
# Criando pipeline

- arquivo de configuracao - .gitlab-ci.yml (yaml)
- estagios (stagy) - cada estagio pode possuir um grupo de jobs que contem atributos (script - obrigatorio)

![alt text](image.png)

Obrigatorio:
- Spript

Nao obrigatorio:
- before_script
- after_script

# Executando

- Subir em um repositorio do Gitlab

-- Gitlab - botao New project

-- botao Creat blank project
(completar formulario)

-- botao Creat project

![alt text](image-1.png)

OBS: caso solicite senha na hora do push. Como criar 
-- User - Preferences

-- Access Tokens 
![alt text](image-2.png)

User: RafaelAraujoCv (nome da conta)
Senha: ... Gerado no token

* No repositorio
-- Visualizar pipeline ir em CI/CD (ou Build) - pipelines e jobs do arquivo em jobs
![alt text](image-3.png)

![alt text](image-4.png)
![alt text](image-5.png)
![alt text](image-6.png)
![alt text](image-7.png)

# | Stages, Jobs e Script |
problema em ter 1 stage com varios jobs - jobs sao executados em paralelo (mesmo tempo), e ideal no pipeline é q tenha 1 jobs em execução caso de erro nao execute o proximo. em paralelo somente jobs e nao seja ligados.
![alt text](image-10.png)
![alt text](image-8.png)

# Dependencias entre Jobs
Em build o push_imagens so pode ser iniciado se o criar_imagens for concluido sem erro

![alt text](image-9.png)

Adicionar o needs:

![alt text](image-11.png)

# Visualizando as dependencias no GitLab
![alt text](image-12.png)

# Visualizando os scripts executados
* pwd - mostra o caminho do repositorio
* ls - mostra pastas e arquivos do caminho
* cat /etc/os-release - distribuição utilizando
* mkdir pipeline - cria repositorio/pasta

![alt text](image-14.png)

Para ver script criar no job

![alt text](image-13.png)

# Outra forma de utilização dos scripts
Criar arquivo script.sh na raiz da pasta projeto

Anotar com #!/bin/bash e colar os comandos 
![alt text](image-15.png)

Em .gitlab-ci.yml chamar o script e dar permição com chmod
![alt text](image-16.png)

# QUIZ

![alt text](image-17.png)
![alt text](image-18.png)
![alt text](image-19.png)
![alt text](image-20.png)

Complementos : 

https://www.redhat.com/pt-br/topics/devops/what-cicd-pipeline

https://www.hostgator.com.br/blog/o-que-e-deploy-e-como-realiza-lo/

# | Criando nova Bransh |
Criar nova bransh via GitLab 

![alt text](image-21.png)

Ou Git code

![alt text](image-22.png)

* git push --set-upstream origin test

Adicionar Only para informar em qual bransh é para o script rodar

![alt text](image-23.png)

# Workflow Rules

Para nao ter que ficar colocando sempre o Only sera criado uma regra. Retirar os Only dos script e adicionar workflow na primeira linha
![alt text](image-24.png)

Ex: SE a variavel for diferente de "main" - nao sera executado nenhum stage da pipe. SE for igual sera executado

# QUIZ
![alt text](image-25.png)
![alt text](image-26.png)
![alt text](image-27.png)
![alt text](image-28.png)

# | Arquitetura GitLab |
Pipe e executada em um Docker com iamimagemem
![alt text](image-29.png)
* Para trocar a imagem, ir em Dock Hub indicando na pipeline pela image:
![alt text](image-30.png)
![alt text](image-31.png)

# Imagem para Docker especifico 
![alt text](image-32.png)

